import dotenv from 'dotenv'
import { argv } from 'yargs'

dotenv.config({
  path: argv.env || '.env',
})

let TABLEAVAIBLE = 0
let BOOKING = []
let INITAIL = false

export default {
  APPNAME: 'Table-Reservation',
  PORT: process.env.PORT || 3000,

  TABLEAVAIBLE,
  BOOKING,
  INITAIL,
}
