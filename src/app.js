import express from 'express'

const app = express()
app.use(express.json())
app.use(
  express.urlencoded({
    extended: true,
  }),
)

/** api routes */
import routers from './routers'
app.use(routers)

import errorHandler from './middlewares/errorHandler'
app.use(errorHandler)

export default app
