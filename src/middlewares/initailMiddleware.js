import constants from '../configs/constants'
import responseCode from '../configs/responseCode'

const initailMiddleware = async (req, res, next) => {
  const { INITAIL } = constants
  if (!INITAIL) {
    return res.status(responseCode.ERROR_API).send({
      message: 'Initailize number of table first'
    })
  }
  next()
}

export default initailMiddleware
