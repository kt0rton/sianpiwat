import responseCode from '../configs/responseCode'

function apiErrorHandler(err, req, res, next) {
  if (err && err.error && err.error.isJoi) {
    console.log('ERROR', err.type)
    res.status(responseCode.ERROR_BAD_REQUEST).json({
      code: responseCode.ERROR_BAD_REQUEST,
      type: err.type,
      message: err.error.toString(),
    })
    return
  }

  console.log(err)
  res.status(responseCode.ERROR_API).json({
    code: responseCode.ERROR_API,
    message: err.message,
  })
}

export default apiErrorHandler
