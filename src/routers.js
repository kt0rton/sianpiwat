import express from 'express'

import tableController from './controllers/tableController'

const router = express.Router()

router.use(tableController)

export default router
