import express from 'express'
import validation from 'express-joi-validation'
import Joi from 'joi'
import moment from 'moment-timezone'
import constants from '../configs/constants'
import responseCode from '../configs/responseCode'

const router = express.Router()
const validator = validation.createValidator({ passError: true })

let TABLEAVAIBLE = constants.TABLEAVAIBLE
let BOOKING = constants.BOOKING
let INITAIL = constants.INITAIL

const checkInitail = (req, res, next) => {
  if (!INITAIL)
    return res.status(responseCode.ERROR_BAD_REQUEST).send({
      message: 'Initailize number of table first',
    })
  next()
}
/**
 * @api {post} /initailiza initail number of table
 */
const intialValidator = Joi.object({
  table: Joi.number().required(),
})
router.post(
  '/initialize',
  validator.body(intialValidator),
  async (req, res, next) => {
    if (INITAIL)
      return res.status(responseCode.ERROR_BAD_REQUEST).send({
        message: 'Already initalize',
      })
    TABLEAVAIBLE = req.body.table
    INITAIL = true
    res.send({
      message: 'Initailize successful',
    })
  },
)

/**
 * @api {get} /table/check Check avaible table
 */
router.get('/table/check', checkInitail, async (req, res, next) => {
  res.send({
    TABLEAVAIBLE,
    BOOKING,
  })
})

const reservationValidator = Joi.object({
  customers: Joi.number().required(),
})
router.post(
  '/table/reservation',
  [checkInitail, validator.body(reservationValidator)],
  async (req, res, next) => {
    const { customers } = req.body
    const tables = Math.ceil(customers / 4) // find number of table
    const available = TABLEAVAIBLE - tables
    if (available < 0) {
      return res.status(400).send({
        message: 'Not enough table for this reservation',
      })
    }
    TABLEAVAIBLE = available
    const bookingData = {
      table: tables,
      customers,
      booking_id: moment().unix(),
      created_at: moment().tz('Asia/Bangkok').toDate(),
    }
    BOOKING.push(bookingData)
    console.log('available', TABLEAVAIBLE)
    res.send({ ...bookingData })
  },
)

/**
 * @api {delete} /table/reserveation Cancel reservation by booking no.
 */
const cancleReservationValidator = Joi.object({
  booking_id: Joi.number().required(),
})
router.delete(
  '/table/reservation',
  [checkInitail, validator.body(cancleReservationValidator)],
  async (req, res, next) => {
    const result = BOOKING.filter(
      (book) => book.booking_id === req.body.booking_id,
    )
    if (result.length == 0) {
      return res.status(responseCode.ERROR_NOT_FOUND).send({
        message: 'Booking not found',
      })
    }

    /** set new available value */
    TABLEAVAIBLE = TABLEAVAIBLE + result[0].table
    console.log('available', TABLEAVAIBLE)
    BOOKING = BOOKING.filter((book) => book.booking_id !== req.body.booking_id)
    res.send({
      message: 'Cancel reservation sucessfull',
      result,
    })
  },
)

export default router
