import app from './app'
import constants from './configs/constants'
import http from 'http'

const { APPNAME, PORT } = constants
const server = http.Server(app)

server.listen(PORT, (err) => {
  console.log(`${APPNAME} is listening on port: ${PORT}`);
});
